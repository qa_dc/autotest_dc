package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {
    // webdriver
    WebDriver driver;
    //selectors
//    public By indexButton = By.cssSelector("a[href='index.html']");
    @FindBy(css ="a[href='index.html']")
    WebElement indexButton;
    @FindBy(css="div.app-navigation a[href='page?app=testpages&t=About']")
    WebElement aboutButton;
    public static final String HOME_URL = "https://testpages.eviltester.com/styled/index.html";
    //contructor


    public HomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }
    //actions
    public void clickIndex(){
        indexButton.click();
    }
    public void clickAbout(){
        aboutButton.click();
    }

}
