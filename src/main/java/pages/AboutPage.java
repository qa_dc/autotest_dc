package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AboutPage {
    WebDriver driver;

    @FindBy(css = "#about-testpages")
    WebElement aboutPageText;

    public static final String ABOUT_URL = "https://testpages.eviltester.com/styled/page?app=testpages&t=About";

    public AboutPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }


    public String checkAboutPageText(){
        return aboutPageText.getText();
    }


}
