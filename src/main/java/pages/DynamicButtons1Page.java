package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DynamicButtons1Page {
    WebDriver driver;
    public static final String DYNAMIC_BUTTONS1_URL = "https://testpages.eviltester.com/styled/dynamic-buttons-simple.html";
    //selectors:
    @FindBy(css = "[id='button00']")
    WebElement startButton;
    @FindBy(css="[id='buttonmessage']")
    WebElement clickText;
    public DynamicButtons1Page(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }
    //checks:
    public  boolean checkStartButtonIsDisplayed(){
        return startButton.isDisplayed();
    }
//    public boolean checkClickTextIsDisplayed(){
//        return clickText.isDisplayed();
//    }
    public String checkClickButtonsText(){
        return clickText.getText();
    }
}
