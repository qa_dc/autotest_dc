@About
Feature: About page tests

  Scenario: Verify navigation to home page
    Given Being on about page
    When Click on Index button
    Then Home page opens "https://testpages.eviltester.com/styled/index.html"
    And Text tile iss "Practice Applications and Pages For Automating and Testing"