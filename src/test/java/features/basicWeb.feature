
Feature: Basic Web Page tests

  Scenario: Verify navigation from Home page to Basic Web Page tests
    Given Being on home page
    When Click on 'Basic Web Page Example' link
    Then Basic Web Page page opens "https://testpages.eviltester.com/styled/basic-web-page-test.html"
    And Test options visible