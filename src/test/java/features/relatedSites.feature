@Related
Feature: Related sites page tests

  Scenario: Verify navigation to about page
    Given Being on related sites page
    When Click on About button
    Then About page opens "https://testpages.eviltester.com/styled/page?app=testpages&t=About"
    And Text tile is "About TestPages"


#https://testpages.eviltester.com/styled/page?app=testpages&t=Others