
Feature: Home page tests

  Scenario: Verify navigation to about page
    Given Being on home page
    When Click on About button
    Then About page opens "https://testpages.eviltester.com/styled/page?app=testpages&t=About"
    And Text tile is "About TestPages"
    And Textd tile is "About TestPagesdf"

