@Home
Feature: Dynamic Button Click Sequence 01

  Scenario: Initial state of the page
    Given I navigate to the dynamic buttons page
    Then I should see the Start button
    And I should see the message "Click all 4 buttons." above the Start button