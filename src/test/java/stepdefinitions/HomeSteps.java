package stepdefinitions;

import io.cucumber.java.PendingException;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.AboutPage;
import pages.HomePage;

import java.util.logging.Level;

import static stepdefinitions.Hooks.driver;
import static stepdefinitions.Hooks.logger;

public class HomeSteps {

    HomePage homePage;
    AboutPage aboutPage;

    @Given("Being on home page")
    public void beingOnHomePage(){
        driver.get(HomePage.HOME_URL);
        logger.log(Level.INFO, "Going to home page");
        homePage = new HomePage(driver);
    }

    @When("Click on About button")
    public void clickOnAboutButton() {
        homePage.clickAbout();
        aboutPage = new AboutPage(driver);
    }

    @Then("About page opens {string}")
    public void aboutPageOpens(String text) {
        Assert.assertEquals("Invalid URL",
                text,
                driver.getCurrentUrl());
    }

    @And("Text tile is {string}")
    public void textTileIs(String text) {
        Assert.assertEquals("Invalid text",
                text,
                aboutPage.checkAboutPageText());
    }


}
