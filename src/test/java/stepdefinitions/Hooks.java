package stepdefinitions;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Hooks {
    protected static WebDriver driver;
    protected static Logger logger = Logger.getLogger(Hooks.class.getName());
    @Before
    public void setupChrome(){
        driver = new ChromeDriver();
        logger.log(Level.INFO, "Opening Chrome browser");
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.manage().window().maximize();
    }
    @After
    public void quitChrome(){
        driver.quit();
        logger.log(Level.INFO, "Closing Chrome browser");
    }

}
