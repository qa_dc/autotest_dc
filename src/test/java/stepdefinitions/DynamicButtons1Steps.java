package stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.junit.Assert;
import pages.DynamicButtons1Page;

import java.util.logging.Level;

import static stepdefinitions.Hooks.driver;
import static stepdefinitions.Hooks.logger;

public class DynamicButtons1Steps {

    DynamicButtons1Page dynamicButtons1Page;

    @Given("I navigate to the dynamic buttons page")
    public void navigateToTheDynamicButtonsPage() {
        dynamicButtons1Page = new DynamicButtons1Page(driver);
        driver.get(DynamicButtons1Page.DYNAMIC_BUTTONS1_URL);
        logger.log(Level.INFO, "Being on Dynamic buttons Page");
    }

    @Then("I should see the Start button")
    public void seeTheButton() {
        Assert.assertTrue("Start button missing",
                dynamicButtons1Page.checkStartButtonIsDisplayed());
    }

    @And("I should see the message {string} above the Start button")
    public void seeTheMessageAboveTheButton(String text) {
        Assert.assertEquals("Text missing",
                text,
                dynamicButtons1Page.checkClickButtonsText());

    }


}
